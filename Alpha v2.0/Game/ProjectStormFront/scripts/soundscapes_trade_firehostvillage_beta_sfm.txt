"Root"
{
	"dsp"	"1"

	"playlooping"
	{
		"volume"	".15"
		"pitch"		"100"
		"wave"		"ambient/forest_life.wav"
	}

	"playlooping"
	{
		"volume"	".18"
		"pitch"		"100"
		"wave"		"ambient/lightwindcreaks.wav"
	}


	"playlooping"
	{
		"volume"	"0.05"
		"pitch"		"100"
		"wave"		"ambient/rain.wav"
	}
	
		"playrandom"
	{
		"time"		"10,35"
		"volume"	"0.75"
		"pitch"		"100"
		"rndwave"
		{
			"wave"		"ambient_mp3/dog1.mp3"
			"wave"		"ambient_mp3/dog2.mp3"
			"wave"		"ambient_mp3/dog3.mp3"
			"wave"		"ambient_mp3/dog4.mp3"
			"wave"		"ambient_mp3/warbler.mp3"
			"wave"		"ambient_mp3/forest_bird6.mp3"
			"wave"		"ambient_mp3/forest_bird7.mp3"
			"wave"		"ambient_mp3/forest_bird1.mp3"
			"wave"		"ambient_mp3/bird2.mp3"
			"wave"		"ambient_mp3/bird3.mp3"
		}
	}
}