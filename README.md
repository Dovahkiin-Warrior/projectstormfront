<h1>PROJECT STORMFRONT ALPHA SERIES</h1>

<b>Welcome to Project: StormFront Alpha. This is a Source 2013 mod that is developed by the kind staff of FireHostRedux.net in which the player survives tornadoes.
The plans for this mod are not currently complete, and they may very over time; hence why it is an alpha mod. The sole purpose of this mod is to give the player a realistic tornado survival experience.
Future versions of this mod will include the ability to properly storm chase, the ability to be killed by falling objects, the ability to be struck by lightning, and so much more.

Creation of this mod was inspired by ThePlagueMistress/LittleDemonCrow of FireHostRedux, in which was done when she remarkably quoted "This map is too laggy and I can't run it." when we were running an experimental map known as "The Tornado Engine".
Continued updates and development are brought to you courtesy of FireHostRedux.net, their partners, and friends.
</b>

<h1> HOW TO PLAY </h1>
<b>First off, install <a href="https://developer.valvesoftware.com/wiki/Source_SDK_2013">Source SDK 2013 Multiplayer</a> (You will need to click Multiplayer to install it directly from this wiki page).

Download the latest release of <a href="https://github.com/Dovahkiin-Warrior/ProjectStormFront">Project: StormFront</a> and extract the zip to your steamapps/sourcemods folder.

Restart Steam and run the game.

In these early stages of development, players will be mostly testing out game features such as tornado physics, vehicles, and the like. Those of you interested in contacting the developers may join FireHostRedux Discord and post anything in #projectstormfront-guest via https://discord.gg/JZ7CeDw .

If you plan to redistribute this game or use it for commercial use, you must agree to the below license agreement. Commercial use at this point in time is not permitted just yet.

</b>
<h2>LICENSE AGREEMENT</h2>
<p>The content here is provided "as is" without warranty. You may not redistribute this content without first opening a discussion on the project page or otherwise contacting the author and you may not use this content for commercial use, as current versions are unstable and may not properly work.
</p>